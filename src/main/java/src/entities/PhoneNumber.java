package src.entities;


public class PhoneNumber {

    // attributs
    private String  kind;
    private String  number;
    private Contact contact;
    private Integer id;

    // constructeurs
    public PhoneNumber() {}

    public PhoneNumber(String _kind, String _number) {
        this.kind   = _kind; 
        this.number = _number;
    }

    // getters et setters
    public Contact getContact     () { return this.contact; }
    public String  getPhoneNumber () { return this.number;  }
    public String  getPhoneKind   () { return this.kind;    }

    public void setContact     (Contact _contact) { this.contact = _contact; }
    public void setPhoneNumber (String  _number)  { this.number  = _number;  }
    public void setPhoneKind   (String  _kind)    { this.kind    = _kind;    }

    // methodes
}
