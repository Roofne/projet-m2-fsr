package src.entities;

public class Adresse {
    // attributs
    private String  street;
    private String  city;
    private String  zip;
    private String  country;
    private Contact contact;
    private Integer id;

    // construteurs
    public Adresse() {}

    public Adresse(String _street, String _city, String _zip, String _country) {
        this.street  = _street;
        this.city    = _city;
        this.zip     = _zip;
        this.country = _country;
    }
    
    // getters
    public Integer getID      () { return this.id;      }
    public String  getStreet  () { return this.street;  }
    public String  getCity    () { return this.city;    }
    public String  getZIP     () { return this.zip;     }
    public String  getCountry () { return this.country; }
    public Contact getContact () { return this.contact; }

    // setters
    public void setID      (Integer _id)      { this.id      = _id;      }
    public void setStreet  (String  _street)  { this.street  = _street;  }
    public void setCity    (String  _city)    { this.city    = _city;    }
    public void setZIP     (String  _zip)     { this.zip     = _zip;     }
    public void setCountry (String  _country) { this.country = _country; }
    public void setContact (Contact _contact) { this.contact = _contact; }
}
