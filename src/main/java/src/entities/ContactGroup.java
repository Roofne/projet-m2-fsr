package src.entities;

import java.util.List;

public class ContactGroup {

    // attributs
    public Integer groupID;
    public String  groupName;
    public List<Contact> contacts;

    // constructeurs
    public ContactGroup() {}
    
    public ContactGroup(String _groupName) {
        this.groupName = _groupName;
    }

    // getters et setters
    public List<Contact> getContact   () { return this.contacts;  }
    public Integer       getGroupID   () { return this.groupID;   }
    public String        getGroupName () { return this.groupName; }

    public void setContact   (List<Contact> _contacts) { this.contacts  = _contacts;  }
    public void setGroupID   (Integer _id)             { this.groupID   = _id;        }
    public void setGroupName (String _groupName)       { this.groupName = _groupName; }

    // methodes
}